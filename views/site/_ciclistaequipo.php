<?php

use yii\helpers\Html;


?>
<div class="col-sm-12" >
    <div class="card alturaminimaciclicta">
        <div class="card-body tarjeta">
           
            <h3 class="tituloEquipo"><?= $model->nombre ?></h3>
             
            <p >Dorsal : <?= $model->dorsal ?>  <br> Edad : <?= $model->edad ?></p>
            <p>
                 <?= Html::a('datos del ciclista', ['site/datosciclista','dorsal'=>$model->dorsal], ['class'=>'btn btn-success'])?>
            </p>
        </div>
    </div>
</div>


