<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'ciclistas de equipo';
?>

<div class="body-content centrar">

    <?=
    ListView::widget([
        'dataProvider' => $datosciclista,
        'itemView' => '_datosciclista',
        'layout' => " \n {items} \n\n{pager}",
        'viewParams' => ["etapasganadasciclista" => $etapasganadasciclista,
            "datosciclista" => $datosciclista,
            "puertosganadasciclista" => $puertosganadasciclista,
            "datospuertos" => $datospuertos,
            "datosetapa" => $datosetapa,
            "datosmaillot" => $datosmaillot,
            "maillotsllevados" => $maillotsllevados,
            
            
        ],
    ]);
    ?>





</div>
</div>