<?php
use yii\bootstrap4\Html;
$this->title = 'Vuelta Ciclista 1994';
?>
<body>
   
    <br>
    <br>
    <br>
        <div id="carouselCiclistas" class="carousel slide" data-ride="carousel">     

            <ol class="carousel-indicators">
                <li data-target="#carouselCiclistas" data-slide-to="0" class="active"></li>
                <li data-target="#carouselCiclistas" data-slide-to="1" class="active"></li>
                <li data-target="#carouselCiclistas" data-slide-to="2" class="active"></li>
            </ol>

    <div class="row justify-content-center">        
            <div class=" col-md-12">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <?= Html::img('@web/img/carrusel1.jpg',['class' => 'd-block w-100' ], ['alt' => 'My logo']) ?>
                    </div>
                    <div class="carousel-item">
                         <?= Html::img('@web/img/carrusel2.jpg',['class' => 'd-block w-100' ], ['alt' => 'My logo']) ?>
                    </div>
                    <div class="carousel-item">
                        <?= Html::img('@web/img/carrusel3.jpg',['class' => 'd-block w-100' ], ['alt' => 'My logo']) ?>
                    </div>
                </div>
                <a href="#carouselCiclistas" class="carousel-control-prev" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a href="#carouselCiclistas" class="carousel-control-next" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
    </div>
</div>
        
</body>
       
    <div class="card text-white bg-dark mb-3 mt-5">
      <div class="card-header">1995 - 1996</div>
      <div class="card-body">
        <h5 class="card-title">Historia </h5>
        <p class="card-text">La Vuelta Ciclista a España está íntimamente ligada a los medios de comunicación. Si en la actualidad sería imposible concebir un acontecimiento deportivo de tal magnitud sin la presencia de los distintos medios, que hacen público hasta el más mínimo detalle de la carrera, la primera edición de la Vuelta, celebrada en 1935, fue posible gracias al esfuerzo del diario Informaciones, organizador del evento.<br>

                Las condiciones en las que se vieron obligados a competir los 50 participantes convirtieron en epopeya aquella primera edición, ganada por el belga Gustave Deloor, tras una enconada lucha con el español Mariano Cañardo, quien padeció todo tipo de desgracias que le impidieron ser el primer vencedor de la Vuelta. La diferencia final entre ambos fue de más de 14 minutos.<br>
                <br>
                Sólo cuatro ciclistas españoles pudieron subir al podio como vencedores de etapa: Escuriet, primer líder de nuestro país en la Historia de la Vuelta, Cañardo, Cardona y Montes.<br>
                <br>
                La delicada situación social y política que vivía España estuvo a punto de impedir la celebración de la segunda edición de la Vuelta y hasta el mismo día de su comienzo no se pudo confirmar el inicio de la ronda española. El esperado duelo entre los dos primeros clasificados del año anterior, Deloor y Cañardo, se vio truncado por una caída del español en la primera etapa. Un perro se cruzó en el camino de Cañardo y éste se fue al suelo, sufriendo múltiples heridas en las piernas y en la cabeza. Pese a ello logró llegar a la meta, aunque sus opciones de liderar la clasificación el último día ya habían desaparecido totalmente.<br>
                <br>
                El belga ya no abandonó el liderazgo y se dedicó a ayudar a su hermano Alphonse para que consiguiera el segundo puesto, algo que finalmente logró. La victoria, segunda consecutiva, de Deloor se vio revalorizada con la presencia de ciclistas de la calidad de Berrendero, revelación en aquella edición, Delio Rodríguez y Vicente y Fermín Trueba.
                <br>
                La Guerra Civil se cruzó bruscamente en la progresión de la Vuelta, que estuvo cuatro años sin poder celebrarse, y en la de las principales estrellas del momento, como Julián Berrendero, quien vivió el enfrentamiento directamente, ya que en aquella época cumplía sus deberes militares.<br>
                <br>
                </p>
        </div>
</div>
