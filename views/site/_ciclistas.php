<?php
use yii\helpers\Html;
use yii\widgets\ListView;
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<div class="col-sm-12" >
    <div class="card alturaminimaciclicta">
        <div class="card-body tarjeta">
           
            <h3 class="tituloEquipo"><?= $model->nombre ?></h3>
             
            <p >Equipo : <?= $model->nomequipo ?><br>Dorsal : <?= $model->dorsal ?>  <br> Edad : <?= $model->edad ?></p>
            <p>
                 <?= Html::a('Datos del ciclista', ['site/datosciclista','dorsal'=>$model->dorsal], ['class'=>'btn btn-success'])?>
            </p>
        </div>
    </div>
</div>
