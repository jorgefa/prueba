<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\widgets\ListView;
?>

<div class="list-view-ciclictasindividual">
    <h2 class="titulo-ciclista">Todos los ciclistas   <?= Html::a('Filtrar por equipo', ['site/equipo'], ['class' => 'btn btn-success']) ?></h2>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <div class="graficas graficaCiclista">      
        <?php
        foreach ($variable as $key => $values) {
            $nomequipo1[] = ($values['nombre']);
            $etapasGanadas1[] = intval($values['etapasGanadas']);
            $puertosganados1[] = intval($variablepuerto[$key]['puertosganados']);
            $maillotsganados1[] = intval($variablemaillot[$key]['maillotsllevados']);            
        }
        echo
        Highcharts::widget([
            'scripts' => ['modules'],
            'options' => [
                'chart' => ['type' => 'area'],
                'title' => ['text' => 'Desempeño de los ciclistas'],
                'xAxis' => ['categories' => $nomequipo1],
                'yAxis' => ['title' => ['text' => 'Ganadas']],
                'series' => [
                    [
                        'name' => 'Etapas',
                        'colorByPoint' => false,
                        'data' => $etapasGanadas1,
                    ],
                    [
                        'name' => 'Puertos',
                        'colorByPoint' => false,
                        'data' => $puertosganados1,
                    ],
                    [
                        'name' => 'Maillots',
                        'colorByPoint' => false,
                        'data' => $maillotsganados1,
                    ],
                ],
            ],
        ]);
        ?>
        </div>
        <?=
        ListView::widget([
            'dataProvider' => $datosciclista,
            'itemView' => '_ciclistas',
            'layout' => " \n {items} \n\n{pager}",
            'itemOptions' => [
                'class' => 'tarjeta-ciclista',
            ],
        ]);
        ?>
    </div>