<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

use yii\helpers\Html;
?>

    <div class="body-content">
        <h1 class="titulo-equipo3">Equipo <?= Html::img('@web/img/' . $nomequipo . '.png', ['alt' => 'My logo']) ?> </h1>
        <h4 class="subtitulorankings">El director <?= $nombredirector ?> dirigia un equipo de <?= $edadmedia ?> años de edad media, con el que consiguio:</h4> <br>   
        <div class="row">
            <div class="col-sm-4" >
                <div class="card alturaminimacarac2">
                    <div class="card-body tarjeta-carac-ciclista">
                        <h3 class="tituloEquipo">Datos Etapas </h3><br>
                        <p>Puesto en el ranking Nº: <?= $rankingequipo ?>.</p>
                        <p>Etapas ganadas totales: <?= $model->eganadas ?>.</p>
                        <?php
                        foreach ($mejorciclistaequipo as $variable) {
                            $nombre1[] = ($variable['nombre']);
                            $edad1[] = ($variable['edad']);
                            $eganadas1[] = ($variable['etapasGanadas']);
                            echo "Mejor ciclista: " . $nombre1[0] . ", edad: " . $edad1[0] . ".<br> Etapas ganadas: " . $eganadas1[0] . ".";
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="col-sm-4" >
                <div class="card alturaminimacarac2">
                    <div class="card-body tarjeta-carac-ciclista">
                        <h3 class="tituloEquipo">Datos Puertos</h3><br>
                        <p>Puesto en el ranking Nº: <?= $rankingequipopuerto ?>.</p>
                        <p>Puertos ganados totales: <?= $puertosganados ?>.</p>
                        <?php
                        foreach ($mejorciclistapuerto as $variable) {
                            $nombre2[] = ($variable['nombre']);
                            $edad2[] = ($variable['edad']);
                            $eganadas2[] = ($variable['puertosganados']);
                            echo "Mejor ciclista: " . $nombre2[0] . ", edad: " . $edad2[0] . ".<br> Puertos ganados: " . $eganadas2[0] . ".";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" >
                <div class="card alturaminimacarac2">
                    <div class="card-body tarjeta-carac-ciclista">
                        <h3 class="tituloEquipo">Datos Maillots </h3><br>
                        <p>Puesto en el ranking Nº: <?= $rankingequipomaillots ?>.</p>
                        <p>Maillots llevados totales: <?= $maillitsganados ?>.</p>
                        <?php
                        foreach ($mejorciclistaMaillot as $variable) {
                            $nombre3[] = ($variable['nombre']);
                            $edad3[] = ($variable['edad']);
                            $eganadas3[] = ($variable['codigo']);
                            echo "Mejor ciclista: " . $nombre3[0] . ", edad: " . $edad3[0] . ".<br> Maillots llevados: " . $eganadas3[0] . ".";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
