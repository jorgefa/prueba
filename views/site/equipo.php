<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
use miloschuman\highcharts\Highcharts;

$this->title = 'equipos';
?>
<div class="row">
    <div class=" body-content">
        <h2 class="titulo-ciclista tituloequipo2">Todos los equipos   <?= Html::a('Filtrar por ciclista', ['site/datosciclistaindividual'], ['class' => 'btn btn-success']) ?></h2>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        
        <div class="graficas graficaAjuste">   
            <?php
            foreach ($variable as $key => $values) {
                $nomequipo1[] = ($values['nomequipo']);
                $etapasGanadas1[] = intval($values['etapasGanadas']);
                $puertosganados1[] = intval($variablepuerto[$key]['puertosganados']);
                $maillotsganados1[] = intval($variablemaillot[$key]['maillotsllevados']);
            }
            echo
            Highcharts::widget([
                'scripts' => ['modules'],
                'options' => [
                    'chart' => ['type' => 'area'],
                    'title' => ['text' => 'Desempeño de los equipos'],
                    'xAxis' => ['categories' => $nomequipo1],
                    'yAxis' => ['title' => ['text' => 'Ganadas']],
                    'series' => [
                        [
                            'name' => 'Etapas',
                            'colorByPoint' => false,
                            'data' => $etapasGanadas1,
                        ],
                        [
                            'name' => 'Puertos',
                            'colorByPoint' => false,
                            'data' => $puertosganados1,
                        ],
                        [
                            'name' => 'Maillots',
                            'colorByPoint' => false,
                            'data' => $maillotsganados1,
                        ],
                    ],
                ],
            ]);
            ?>
            </div>
            
         
            <?=
            ListView::widget([
                'dataProvider' => $resultado,
                //'ciclistaProvider'=>$mejorCiclistaEquipo,
                'itemView' => '_eindividual',
                'layout' => " \n {items} \n\n{pager}",
                //'img'=>$img,
                'itemOptions' => [
                    'class' => 'list-view-equipos',
                ],
            ]);
            ?>

        </div>
    </div>
